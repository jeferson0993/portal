import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'app': environment.apiKey })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(endPoint: string): Observable<any> {
    return this.http.get(`${environment.apiBaseUrl}${endPoint}`, HTTP_OPTIONS);
  }

  post(endPoint: string): Observable<any> {
    return this.http.post(`${environment.apiBaseUrl}${endPoint}`, HTTP_OPTIONS);
  }
  
  put(endPoint: string): Observable<any> {
    return this.http.put(`${environment.apiBaseUrl}${endPoint}`, HTTP_OPTIONS);
  }

  delete(endPoint: string): Observable<any> {
    return this.http.delete(`${environment.apiBaseUrl}${endPoint}`, HTTP_OPTIONS);
  }

  postWithParam(endPoint: string, param: any): Observable<any> {
    return this.http.post(`${environment.apiBaseUrl}${endPoint}`, param, HTTP_OPTIONS);
  }

  putWithParam(endPoint: string, param: any): Observable<any> {
    return this.http.put(`${environment.apiBaseUrl}${endPoint}`, param, HTTP_OPTIONS);
  }

}
