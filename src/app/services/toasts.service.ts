import { Injectable } from '@angular/core';

import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class ToastsService {
  override = {
    timeOut: 3000,
    closeButton: false
  };

  constructor(private toastrService: ToastrService) { }

  showToast(title: string, message: string, type: string): void {
    switch (type) {
      case 'error':
        this.toastrService.error(message, title, this.override);
        break;
      case 'info':
        this.toastrService.info(message, title, this.override);
        break;
      case 'warning':
        this.toastrService.warning(message, title, this.override);
        break;
      case 'success':
        this.toastrService.success(message, title, this.override);
        break;
      default:
        this.toastrService.show(message, title, this.override);
        break;
    }
  }

}
