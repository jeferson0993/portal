import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable({ providedIn: 'root' })
export class HttpconfigInterceptor implements HttpInterceptor {

  constructor(private token: TokenService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const TOKEN = this.token.getToken();
    if (TOKEN) {
      const AUTH_REQ = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${TOKEN}`)
      });
      return next.handle(AUTH_REQ);
    }
    return next.handle(request);
  }
}
