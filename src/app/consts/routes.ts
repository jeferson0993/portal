export enum routes {
  DASHBOARD = '/dashboard',
  PROFILE = '/profile',
  LOGIN = '/login'
}
