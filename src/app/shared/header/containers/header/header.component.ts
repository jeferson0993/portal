import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { UserService, AuthService } from '../../../../pages/auth/services';
import { routes } from '../../../../consts';
import { ToastsService } from 'src/app/services/toasts.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{
  
  @Input() isMenuOpened: boolean = false;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  
  public user: any = '';
  public routers: typeof routes = routes;

  constructor(
    private userService: UserService,
    private token: TokenService,
    private auth: AuthService,
    private router: Router,
    private toast: ToastsService
  ) {
  }
  ngOnInit(): void {
    this.user = this.userService.getUser();
  }

  public openMenu(): void {
    this.isMenuOpened = !this.isMenuOpened;
    this.isShowSidebar.emit(this.isMenuOpened);
  }

  public signOut(): void {
    this.auth.signOut().subscribe(
      resp => {
        this.token.signOut();
        this.userService.signOut();
        this.toast.showToast('', `volte sempre ${this.user.name}`, 'info');
        this.router.navigate([this.routers.LOGIN]);
      },
      error => {
        this.toast.showToast('', error.message, 'error');
      }
    );
  }

}
