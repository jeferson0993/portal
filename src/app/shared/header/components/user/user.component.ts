import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { routes } from '../../../../consts';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {
  
  @Input() user: any;
  @Output() signOut: EventEmitter<void> = new EventEmitter<void>();

  public routes: typeof routes = routes;

  constructor(private router: Router) {}

  public signOutEmit(): void {
    this.signOut.emit();
  }

  public goToProfile(): void {
    this.router.navigate([this.routes.PROFILE]);
  }

}
