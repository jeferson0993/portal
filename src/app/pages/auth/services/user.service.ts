import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

const USER = 'user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public getUser(): any {
    return JSON.parse(window.sessionStorage.getItem(USER) || '{}');
  }

  public setUser(user: any): void {
    window.sessionStorage.setItem(USER, JSON.stringify(user));
  }

  signOut(): void {
    window.sessionStorage.removeItem(USER);
  }

}
