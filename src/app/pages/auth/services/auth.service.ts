import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { ApiService } from "src/app/services/api.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api: ApiService) {}

  public login(param: any): Observable<any> {
    return this.api.postWithParam('/auth/login', param);
  }

  public sign(): void {
  }

  public signOut(): Observable<any> {
    return this.api.delete('/auth/logout');
  }

  public getUser(): Observable<any> {
    return of(this.api.get('/auth/profile')
    .subscribe(
      user => {
        console.log(user.data.people);    
        return user.data.people;
      },
      error => {
        console.error(error.message);
        return { name: error.message };
      }
    ))
  }
}
