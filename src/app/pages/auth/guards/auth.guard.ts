import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { TokenService } from "src/app/services/token.service";
import { routes } from '../../../consts';

@Injectable()
export class AuthGuard implements CanActivate{
  public routers: typeof routes = routes;

  constructor(private router: Router, private token: TokenService) {
  }

  canActivate(): boolean {
    const token = this.token.getToken();

    if (token !== null) {
      return true;
    } else {
      this.router.navigate([this.routers.LOGIN]);
    }

    return false;
  }
}
