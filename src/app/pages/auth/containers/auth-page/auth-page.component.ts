import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ToastsService } from '../../../../services/toasts.service';

import { AuthService, UserService } from '../../services';
import { routes } from '../../../../consts';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent {

  public routers: typeof routes = routes;

  constructor(
    private service: AuthService,
    private token: TokenService,
    private userService: UserService,
    private toast: ToastsService,
    private router: Router
  ) { }

  public sendLoginForm(loginData: any): void {
    this.service.login(loginData).subscribe(
      resp => {
        this.token.saveToken(resp.data.token);
        this.userService.setUser(resp.data.user.people);
        this.showToast('', `Bem vindo ${resp.data.user.people.name}!`, 'success');
        this.router.navigate([this.routers.DASHBOARD]).then();
      }, error => {
        this.showToast('', error.message, 'error');
      }
    );
  }

  public sendSignForm(): void {
  }

  showToast(title: string, msg: string, type: string) {
    this.toast.showToast(title, msg, type);
  }

}
