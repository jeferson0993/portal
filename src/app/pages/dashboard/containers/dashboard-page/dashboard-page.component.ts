import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services';

import { ToastsService } from 'src/app/services/toasts.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  public auctions$!: Observable<any>;

  constructor(
    private service: DashboardService, 
    private toastrService: ToastsService) {

  }

  ngOnInit(): void {
    this.auctions$ = this.service.loadAuctionsData();
  }

  showSuccess(): void {
    this.toastrService.showToast('', '', 'info');
  }

}
