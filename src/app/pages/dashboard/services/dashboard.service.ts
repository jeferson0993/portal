import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  
  constructor(private api: ApiService) {}

  public loadAuctionsData(): Observable<any> {
    return this.api.get('/auctions');
    /* return of({ acctionsData: [] }); */
  }
}
